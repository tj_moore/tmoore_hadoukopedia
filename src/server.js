var http = require('http'),
	characters = require('./data'),
	db = require('./db'),
	app = require('./app')(characters);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});