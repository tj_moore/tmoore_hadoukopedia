///////////////
/*Characters*/
function toggleInfo(id) {
	$(".singleChar").hide();
	$("." + id).css("background-color" ,"#000");
	$("#" + id).slideDown(800);
	$('html,body').animate({ scrollTop: $('#' + id).offset().top + 15}, 'slow');
}

function charClose(id) {
	$("." + id).css("background-color" ,"#333");
	$("." + id).fadeIn(1200);
	$("#" + id).slideUp(800);
	$('html,body').animate({ scrollTop: $('.' + id).offset().top }, 'slow');
}

function charBio(id) {
	$('.attacks'+id).hide();
	$('.costumes'+id).hide();
	$('.charInfo'+id).fadeIn(800);
}

function charAttacks(id) {
	$('.charInfo'+id).hide();
	$('.costumes'+id).hide();
	$('.attacks'+id).fadeIn(800);
}

function charCostumes(id) {
	$('.charInfo'+id).hide();
	$('.attacks'+id).hide();
	$('.costumes'+id).fadeIn(800);
}

function changeColor(i){
	$('#mainImage').attr('src', ''+i+'');
}

///////////
/*Stages*/
function toggleStage(id) {
	$(".singleStage").hide();
	$("." + id).css("background-color" ,"#000");
	$("." + id).fadeIn(800);
	$("#" + id).fadeIn(800);
}

function stageClose(id) {
	$("." + id).width( "20%" );
	$("." + id).css("background-color" ,"#333");
	$("#" + id).fadeOut(800);
}

////////////
/*Matches*/
function toggleMatch(id) {
	$(".singleMatch").hide();
	$("." + id).slideDown(800);
	$("#" + id).slideDown(800);
}

function matchClose(id) {
	$("." + id).width( "20%" );
	$("." + id).css("background-color" ,"#333");
	$("#" + id).slideUp(800);
}

//
$(document).ready(function () {
	 /////////////
	/*Login Box*/
	$('a.login-window').click(function () {
		var loginBox = $(this).attr('href');
		$(loginBox).fadeIn(500);
		$('.register-popup').hide();
		$('body').append('<div id="mask"></div>');
		$('#mask').fadeIn(500);
		$('#logUser').focus();
		
		var popMargTop = ($(loginBox).height() + 24) / 2; 
		var popMargLeft = ($(loginBox).width() + 24) / 2; 

		$(loginBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		return false;
	});

	$('a.close, #mask').live('click', function () {
		$('#mask , .login-popup').fadeOut(500, function () {
			$('#mask').remove();
		});
		return false;
	});
	
	/////////////////
	/*Register Box*/
	$('a.register-window').click(function () {
		var registerBox = $(this).attr('href');
		$(registerBox).fadeIn(500);
		$('.login-popup').hide();
		$('body').append('<div id="mask"></div>');
    	$('#mask').fadeIn(500);
		$('#regUser').focus();
		
		var popMargTop = ($(registerBox).height() + 24) / 2; 
		var popMargLeft = ($(registerBox).width() + 24) / 2; 

		$(registerBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		return false;
	});
	
	$('a.close, #mask').live('click', function () {
		$('#mask , .register-popup').fadeOut(500, function () {
			$('#mask').remove();
		});
		return false;
	});
	
	//////////////
	/*Edit User*/
	$('a.updateProfile').click(function () {
		var editProfile = $(this).attr('href');
		$(editProfile).fadeIn(500);
		$('body').append('<div id="mask"></div>');
    	$('#mask').fadeIn(500);
		$('#editName').focus();
		
		var popMargTop = ($(editProfile).height() + 24) / 2; 
		var popMargLeft = ($(editProfile).width() + 24) / 2; 

		$(editProfile).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		return false;
	});
	
	$('a.close, #mask').live('click', function () {
		$('#mask , .editProfile').fadeOut(500, function () {
			$('#mask').remove();
		});
		return false;
	});
	
	//////////////
	/*Slideshow*/
	$(function () {		
		$('.img-wrap img:gt(0)').hide();

		$('.next').click(function () {
			$('.img-wrap img:first-child').hide().next().toggle("slide", {direction: "right" }).end().appendTo('.img-wrap');
		});

		$('.prev').click(function () {
			$('.img-wrap img:first-child').hide();
			$('.img-wrap img:last-child').prependTo('.img-wrap').fadeOut();
			$('.img-wrap img:first-child').toggle("slide", {direction: "left" }, 600);
		});
	});
	
	//////////////////////////
	/*Select Box Calculator*/
	$("#selectChar").change(function(){
		var selectedIndex = $("#selectChar")[0].selectedIndex;

		$('ul#addImages li').each(function(index) {
			if ( index === selectedIndex ) { $(this).show(); }
			else { $(this).hide(); }
		});
	});
	
	////////////////////////
	/*Select Box Costumes*/
	$("#selectCostume").prop("selectedIndex", -1);
	$("#selectCostume").change(function(){
		var selectedIndex = $("#selectCostume")[0].selectedIndex;
		$(".costumeImage").hide();
		$('ul#costumeImages li').each(function(index) {
			if ( index === selectedIndex ) { 
				$(this).show(); 
			}
			else { 
				$(this).hide(); 
			}
		});
	});
});

///////////
/*Search*/
$(function () {
  $("#search-query").autocomplete({
      source: function (request, response) {
         $.ajax({
            url: "/search_char",
            type: "GET",
            data: request, 
            success: function (data) {
               response($.map(data, function (el) {
                  return {
                     label: el.name,
                     value: el._id
                  };
                  }));
               }
            });
         },
         minLength: 2,
         focus: function (event, ui) { 
            this.value = ui.item.label; 
            event.preventDefault();
         },
         select: function (event, ui) {
            this.value = ui.item.label;
            $(this).next("input").val(ui.item.value);          
            event.preventDefault();
            $('#quicksearch').submit();
         }
  });

});















