var path = require('path'),
    express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    flash = require('connect-flash'),
    engine = require('ejs-locals'),
    mongoose = require('mongoose'),
    passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy;
//    configDB = require('./db'),

var charModel = require('./models/character');
var stageModel = require('./models/stage');
var User = require('./models/user');

var db = mongoose.connect('mongodb://127.0.0.1:27017/hadoukopedia');

mongoose.connection.once('connected', function(){
	console.log("connected BRAH");
});

//require('./config/passport')(passport);

app.configure(function () {
	app.use(express.logger('dev'));
	app.use(express.cookieParser());
	app.use(express.bodyParser());
	app.engine('ejs', engine);
	app.set('view engine', 'ejs');
	app.use(express.session({
		secret: 'hadoukopedia rocks'
	})); 
	
	app.use(passport.initialize());
	app.use(passport.session());
	
	app.use(flash());
	app.use(app.router);

	app.use(express.static(path.join(__dirname, '/Assets')));
	app.use(express.favicon(path.join(__dirname, "/Assets/images/Icons/favicon.ico")));

});

passport.use(new LocalStrategy(User.authenticate()));

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

function buildResultSet(docs){
	var result = [];
	for(var object in docs){
		result.push(docs[object]);
	}
	return result;
}

app.get('/search_char', function(req, res) {
   var regex = new RegExp(req.query["term"], 'i');
   var query = charModel.find({name: regex}, { 'name': 1 }).sort({"updated_at":-1}).sort({"created_at":-1}).limit(20);
  query.exec(function(err, chars) {
      if (!err) {
         var result = buildResultSet(chars);
         res.send(result, {
            'Content-Type': 'text/html'
         }, 200);
      } else {
         res.send(JSON.stringify(err), {
            'Content-Type': 'text/html'
         }, 404);
      }
   });
});


/*Beginning of Character information*/

app.get('/stages', function (req, res) {
	return stageModel.find(function (err, stages) {
		if (!err) {
			res.render('stages', {
				stages: stages,
				user: req.user,
				title: 'Stages'
			});
		} else {
			return console.log(err);
		}
	});
});

require('./routes/index.js')(app, passport);

app.get('/profile/:id', function (req, res) {
	return User.findById(req.params.id, function (err, user) {
		res.render('addMatch', {
			title: 'Add a Match',
			user: req.user
		});
	});
});
app.post('/profile/addMatch', function (req, res) {
	return User.findById(req.params.id, function (err, user) {
		user.matches._id = req.user._id;
		user.matches.rounds = req.body.rounds;
		user.matches.perfect = req.body.perfect;
		user.matches.chip = req.body.chip;
		user.matches.sCombo = req.body.sCombo;
		user.matches.uCombo = req.body.uCombo;
		return user.save(function (err) {
			if (!err) {
				console.log("updated");
			} else {
				console.log(err);
			}
			res.redirect('/profile', 301);
		});
	});
});

app.get('/about', function (req, res) {
	res.render('about', {
		title: 'about',
		user: req.user,
		isAuthenticated: req.isAuthenticated()
	})
});
app.get('/purchase', function (req, res) {
	res.render('purchase', {
		title: 'purchase',
		user: req.user,
		isAuthenticated: req.isAuthenticated()
	})
});

function checkAuth(req, res, next) {
	if (!req.session.login) {
		res.redirect("/index.ejs");
		return;
	}
	//logged in
	next();
}

app.listen(port);
console.log('The hype happens on port ' + port);