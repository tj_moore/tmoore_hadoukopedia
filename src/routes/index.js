module.exports = function (app) {
	var passport = require('passport'),
		User = require('../models/user'),
		Match = require('../models/match'),
		Char = require('../models/character'),
		moment = require('moment');

	app.get('/', function (req, res) {
		res.render('index.ejs', {
			user: req.user,
			title: "Hadoukopedia"
		});
	});
	
	app.get('/error', function(req, res){
		res.render('error', {user: req.user });
	});

	app.get('/layout', function (req, res) {
		res.render('layout', { user: req.user });
	});
	
	app.get('/characters', function (req, res) {
		return Char.find(function (err, chars) {
			if (!err) {
				res.render('characters.ejs', {
					chars: chars,
					user:req.user,
					title: 'Characters'
				});
			} else {
				return console.log(err);
			}
		});
	});

	app.post('/login', passport.authenticate('local'), function(req, res){
		res.redirect('/profile');
	});

	app.get('/logout', function (req, res) {
		req.logout();
		res.redirect('/');
	});

	app.post('/register', function(req, res) {
		User.register(new User({ 
			username : req.body.username, 
			quote : req.body.quote, 
			bio : req.body.bio }), 
			req.body.password, function(err, user) {
            if (err) {
                return res.render('/error', {user:user});
            }else{
            	res.redirect('/profile');
			}
        });
	});

	app.get('/profile', isLoggedIn, function (req, res) {
		return Match.find(function (err, matches) {
			return Char.find(function (err, chars) {
				res.render('profile', {
					chars: chars,
					matches: matches,
					user: req.user
				});
			});
		}).populate('charUsed');
	});

	app.get('/matches', function (req, res) {
		return Match.find(function (err, matches) {
			return Char.find(function (err, chars) {
				if (!err) {
					res.render('matches', {
						chars: chars,
						user: req.user,
						matches: matches,
						title: 'Matches'
					});
				} else {
					return console.log(err);
				}
			});
		}).populate('charUsed');
	});

	app.get('/addMatch', function (req, res) {
		return Char.find(function (err, chars) {
			if (!err) {
				res.render('addMatch', {
					chars: chars,
					user:req.user,
					title: 'Add a Match'
				});
			} else {
				return console.log(err);
			}
		});
	});

//	app.post('/addMatch', function (req, res) {
//		var matchData;
//		var date = moment(Date.now()).format('MMMM Do YYYY, h:mm:ss');
//		User.findById(req.params.id, function (err, user) {
//			matchData = new Match({
//				rounds: req.body.rounds,
//				perfect: req.body.perfect,
//				chip: req.body.chip,
//				sCombo: req.body.sCombo,
//				uCombo: req.body.uCombo,
//				characterUsed: req.body.characterUsed,
//				date: date
//			});
//			return matchData.save(function (err) {
//				if (!err) {
//					console.log("updated");
//				} else {
//					console.log(err);
//				}
//				res.redirect('/profile', 301);
//			});
//		});
//	});
	app.post('/addMatch', function (req, res) {
		var matchData;
		var date = moment(Date.now()).format('MMMM Do YYYY, h:mm:ss');
		matchData = new Match({
			endResult: req.body.endResult,
			rounds: req.body.rounds,
			regularWin: req.body.regularWin,
			perfect: req.body.perfect,
			chip: req.body.chip,
			sCombo: req.body.sCombo,
			uCombo: req.body.uCombo,
			charUsed: req.body.characterUsed,
			date: date,
		});
		return matchData.save(function (err) {
			if (!err) {
				console.log(req.body.characterUsed);
			} else {
				console.log(err);
			}
			res.redirect('/', 301);
		});
	});
};

function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()){
		return next();
	}
	res.redirect('/');
}