var mongoose = require("mongoose"),
	Character = require('./character'),
	Schema = mongoose.Schema,
	ObjectId = Schema.Types.ObjectId;

var Match = Schema({
	endResult: {type:String},
	rounds: {type: Number, default: 1},
	perfect: {type: Number, default: 0},
	chip: {type: Number, default: 0},
	sCombo: {type: Number, default: 0},
	uCombo: {type: Number, default: 0},
	charUsed: {type: ObjectId, ref:'Character'},
	date: String
});

module.exports = mongoose.model("Match", Match);