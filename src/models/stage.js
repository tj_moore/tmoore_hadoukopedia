var mongoose = require('mongoose');

var Stage = mongoose.Schema({
	location:String,
	name:String,
	desc:String,
	splash: String
});

module.exports = mongoose.model('Stage', Stage);