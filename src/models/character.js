var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Character = Schema({
	name: String,
	quote: String,
	bio: String,
	splash: String,
	art: String,
	tips: [String],
	attacks:[Attack],
	costumes:[Costume]
});

var Attack = Schema({
	name: String,
	lightDamage: Number,
	mediumDamage: Number,
	heavyDamage: Number,
	exDamage: Number
//	btnCommand: String
});

var Costume = Schema({
	name:String,
	image:String,
	colors:[String]
});

module.exports = mongoose.model('Character', Character);