var mongoose = require('mongoose'),
	Character = require('./character'),
	passportLocalMongoose = require('passport-local-mongoose');

var userSchema = mongoose.Schema({
	username: String,
	quote: String,
	bio: String,
	matches: [Match]
});

var Match = mongoose.Schema({
	endResult: Boolean,
	rounds: {type: Number, max: 7, min: 1},
	regularWin: {type: Number, max: 4, min: 0},
	perfect: {type: Number, max: 4, min: 0},
	chip: {type: Number, max: 4, min: 0},
	sCombo: {type: Number, max: 4, min: 0},
	uCombo: {type: Number, max: 4, min: 0},
	characterID: {type: mongoose.Schema.ObjectId, ref:'Character'},
	characterUsed: {type:String, ref:'Character'},
	date: String
});

userSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model('User', userSchema);

